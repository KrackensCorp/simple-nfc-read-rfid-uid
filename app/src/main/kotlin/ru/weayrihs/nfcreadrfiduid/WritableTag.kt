package ru.weayrihs.nfcreadrfiduid

import android.nfc.FormatException
import android.nfc.Tag
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable
import android.util.Log
import com.google.common.base.Strings.*
import java.util.*

class WritableTag @Throws(FormatException::class) constructor(tag: Tag?) {

	private val ndef: Ndef?
	private val ndefFormatable: NdefFormatable?

	init {
		val technologies = tag!!.techList
		val tagTechs = listOf(*technologies)
		if (tagTechs.contains(Ndef::class.java.canonicalName)) {
			Log.i("TAG", "contains ndef")
			ndef = Ndef.get(tag)
			ndefFormatable = null
		} else if (tagTechs.contains(NdefFormatable::class.java.canonicalName)) {
			Log.i("TAG", "contains ndef_formatable")
			ndefFormatable = NdefFormatable.get(tag)
			ndef = null
		} else {
			throw FormatException("Tag doesn't support ndef")
		}
	}
	private fun isNullOrEmptyCustom(array: ByteArray?): Boolean {
		if (array == null) {
			return true
		}
		val length = array.size
		for (i in 0 until length) {
			if (array[i].toInt() != 0) {
				return false
			}
		}
		return true
	}

	val tagId: String?
		get() {
			if (ndef != null) {
				return bytesToHexString(ndef.tag.id)
			} else if (ndefFormatable != null) {
				return bytesToHexString(ndefFormatable.tag.id)
			}
			return null
		}

	private fun bytesToHexString(src: ByteArray): String? {
		if (isNullOrEmptyCustom(src)) return null
		val sb = StringBuilder()
		for (b in src) sb.append(String.format("%02X", b))
		return sb.toString()
	}
}