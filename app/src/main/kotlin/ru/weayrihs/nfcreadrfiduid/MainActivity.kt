package ru.weayrihs.nfcreadrfiduid

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.nfc.FormatException
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.nfc.Tag
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

	private var adapter: NfcAdapter? = null
	var tag: WritableTag? = null
	var tagId: String? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		val nfcManager = getSystemService(Context.NFC_SERVICE) as NfcManager
		adapter = nfcManager.defaultAdapter

	}

	override fun onResume() {
		super.onResume()
		try {
			val intent = Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
			val nfcPendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
			adapter?.enableForegroundDispatch(this, nfcPendingIntent, null, null)
		} catch (ex: IllegalStateException) {
			Log.e("TAG", "Error enabling NFC foreground dispatch", ex)
		}
	}

	override fun onPause() {
		try {
			adapter?.disableForegroundDispatch(this)
		} catch (ex: IllegalStateException) {
			Log.e("TAG", "Error disabling NFC foreground dispatch", ex)
		}
		super.onPause()
	}

	override fun onNewIntent(intent: Intent) {
		super.onNewIntent(intent)
		try {
			tag = WritableTag(intent.getParcelableExtra<Tag>(NfcAdapter.EXTRA_TAG))
			tagId = tag!!.tagId
			val textView = findViewById<TextView>(R.id.read_tag)
			textView.text = tagId
			Log.e("TAG", "Tag tapped: $tagId")
			Toast.makeText(this, "Tag tapped: $tagId", Toast.LENGTH_SHORT).show()
		} catch (e: FormatException) {
			Log.e("TAG", "Unsupported tag tapped", e)
			return
		}
	}
}